let intCodeProgramOrig = [1,0,0,3,1,1,2,3,1,3,4,3,1,5,0,3,2,1,10,19,2,9,19,23,2,23,10,27,1,6,27,31,1,31,6,35,2,35,10,39,1,39,5,43,2,6,43,47,2,47,10,51,1,51,6,55,1,55,6,59,1,9,59,63,1,63,9,67,1,67,6,71,2,71,13,75,1,75,5,79,1,79,9,83,2,6,83,87,1,87,5,91,2,6,91,95,1,95,9,99,2,6,99,103,1,5,103,107,1,6,107,111,1,111,10,115,2,115,13,119,1,119,6,123,1,123,2,127,1,127,5,0,99,2,14,0,0];
let intCodeProgram = intCodeProgramOrig.slice(0);
let nounVerb = [0,0];
let opcodePos = 0;

function opcode1 (position1, position2, position3) {
    intCodeProgram[position3] = intCodeProgram[position1] + intCodeProgram[position2];
    opcodePos = opcodePos + 4;
}

function opcode2 (position1, position2, position3) {
    intCodeProgram[position3] = intCodeProgram[position1] * intCodeProgram[position2];
    opcodePos = opcodePos + 4;
}

function calculateOp(noun, verb){
    intCodeProgram[1] = noun;
    intCodeProgram[2] = verb;
    while (intCodeProgram[opcodePos] != 99) {
        if (intCodeProgram[opcodePos] === 1) {
            opcode1(intCodeProgram[opcodePos+1], intCodeProgram[opcodePos+2], intCodeProgram[opcodePos+3]);
        } else {
            opcode2(intCodeProgram[opcodePos+1], intCodeProgram[opcodePos+2], intCodeProgram[opcodePos+3]);
        }
        if (intCodeProgram[0] === 19690720) {
            nounVerb[0]=noun;
            nounVerb[1]=verb;
        }
    }
    return intCodeProgram[0];
}


function findAnswer() {
        for (noun = 0; noun < 100; noun++) {
            opcodePos = 0;
            for (verb = 0; verb < 100; verb++) {
                opcodePos = 0;
                intCodeProgram = intCodeProgramOrig.slice(0);
                calculateOp(noun, verb);
            }
        }
}

// console.log("START PROGRAM");
// console.dir(intCodeProgram,  {'maxArrayLength': null} );

findAnswer();

// console.log("FINAL PROGRAM");
// console.dir(intCodeProgram,  {'maxArrayLength': null} );
console.log("Position 1 noun = " + nounVerb[0]);
console.log("Position 2 verb = " + nounVerb[1]);
let answer = 100 * nounVerb[0] + nounVerb[1]
console.log("Answer: " + answer);